//App.js

import { Oval } from "react-loader-spinner";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFrown } from "@fortawesome/free-solid-svg-icons";
import "./App.css";
import { HTTPget } from "./useFetch";

function OuaideurApp() {
  const [input, setInput] = useState("");
  const [weather, setWeather] = useState({
    loading: false,
    data: {},
    error: false,
  });

  const fetchData = async () => {
    setWeather({ loading: true });
    let data = await HTTPget("/city/abidjan");
    setWeather({ loading: false, data: data, error: false });
    console.log(data.location.country);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="App">
      <h1 className="app-name">OUAIDEUR APPS</h1>

      <div className="search-bar">
        <input
          type="text"
          className="city-search"
          placeholder="Enter City Name.."
          name="query"
          value={input}
          onChange={(event) => setInput(event.target.value)}
        />
      </div>
      {weather.loading && (
        <>
          <br />
          <br />
          <Oval type="Oval" color="black" height={100} width={100} />
        </>
      )}
      {weather.error && (
        <>
          <br />
          <br />
          <span className="error-message">
            <FontAwesomeIcon icon={faFrown} />
            <span style={{ fontSize: "20px" }}>City not found</span>
          </span>
        </>
      )}
      {!weather.loading && weather.data.data && (
        <div>
          <div className="city-name">
            <h2>Pays: {weather.data.data.location.country}</h2>
          </div>
        </div>
      )}
    </div>
  );
}

export default OuaideurApp;
