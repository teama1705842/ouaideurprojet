# Cahier de charges

## Groupe d'utilisateurs ?

- randonneur
- voyageur
- athlète
- chasseur
- scientifique

## Contexte et User Stories

- Prévision:

  - En tant que randonneur ou voyageur, je veux pouvoir vérifier la météo dans une destination prévue avant de partir afin d'être bien équipé pour le voyage.
  - En tant que randonneur ou athlète, je veux recevoir des prévisions météorologiques précises pour les zones où je prévois de faire de la randonnée afin de planifier en conséquence.
  - En tant que randonneur, voyageur, athlète ou chasseur, je veux avoir accès à des prévisions météorologiques détaillées pour mon itinéraire afin de plannifier mes voyages en toute sécurité.

- Historique:

  - En tant que scientifique, je veux garder un historique des conditions météorologiques sur une période spécifique (par exemple plusieurs semaines ou mois) pour une région donnée afin de surveiller les tendances météorologiques.

- Consulter des données en temps réél
  - En tant qu'athlète, je veux enregistrer les conditions météorologiques locales pendant des activités sportives (par exemple la course à pied, le cyclisme) afin de suivre et analyser comment la météo peut affecter la performance.
- Comparatif
  - En tant que voyageur ou randonneur, je veux comparer les conditions météorologiques entre plusieurs régions afin de choisir le meilleur endroit a visiter.

autant que ... je veux ... afin de ...
