## INTRODUCTION

Création d'une application de météo simple, l'utilisateur entre le nom de la ville et on retournera des informations concernants la météo

## TECHNOLOGIES

Front -> Angular
Back -> Python/Flask

## UTILISATEURS

Personnes interessées par des activités à l'exterieur et suceptibles aux changements climatiques

## L'ARCHITECTURE

Front-Back

FeatureFlow

régles de nommage: TYPE-FRONT/BACK-NUMERO

TYPE: Feature par exemple, description du type de changement (FEATURE-BUGFIX-REFACTOR)

FRONT/BACK: Selon le scope du changement (front ou back)

NUMERO: numéro du ticket
