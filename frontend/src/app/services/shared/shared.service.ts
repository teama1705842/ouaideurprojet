import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  private villeSubject = new Subject<string>();

  setVille(ville: string) {
    this.villeSubject.next(ville);
  }

  getVille() {
    return this.villeSubject.asObservable();
  }
}
