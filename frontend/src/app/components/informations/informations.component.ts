import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared/shared.service';
import { FormComponent } from 'src/app/components/form/form.component';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.component.html',
  styleUrls: ['./informations.component.css'],
})
export class InformationsComponent implements OnInit {
  ville: string = '';

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    fetch(`http://localhost:5000/city/${this.ville}`)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
      });
  }
}
