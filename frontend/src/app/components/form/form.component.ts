import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared/shared.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  formData: any = {};
  ville: string = '';

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  submit(event: Event) {
    console.log(`ville: ${this.ville}`);
    event.preventDefault();
    console.log(this.formData.ville);
    this.sharedService.setVille(this.formData.ville);
  }
}
