from flask import Flask
import requests, json
from flask_cors import CORS
import pytest

app = Flask(__name__)

CORS(app)

@app.route('/city/<city>')
def city(city):
    
    # Enter your API key here
    api_key = "69aad0260c6d425a99e124410231710"
    
    # base_url variable to store url
    base_url = "http://api.weatherapi.com/v1/current.json?"
    
    # Give city name
    city_name = city
    
    # complete_url variable to store
    # complete url address
    complete_url = f"{base_url}key={api_key}&q={city_name}&aqi=no"
    
    # get method of requests module
    # return response object
    response = requests.get(complete_url)
    
    # json method of response object 
    # convert json format data into
    # python format data
    responseJSON = response.json()

    return responseJSON
 

# IMPORTANT, GARDER LE HOST EN TANT QUE 0.0.0.0
if __name__ == "__main__":
    app.run('0.0.0.0', 5000, True)
 